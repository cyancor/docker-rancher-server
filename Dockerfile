FROM rancher/server:stable
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

VOLUME ["/var"]